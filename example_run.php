<?php
    require_once "vendor/autoload.php";
    require_once "vladapps/jobsbg/src/loader.php";

    use GuzzleHttp\Client;
    use VladApps\Jobsbg\GuzzleJobsbgClient;

    $jobsbgClient = new GuzzleJobsbgClient(new Client(['cookies' => true]), 'asd@g.com', '123456');

    // $res = $jobsbgClient->userLogin();
    $res = $jobsbgClient->jobsListing();

    print_r($res->jobListing()->getAll());
    print_r($res->companyListing()->getAll());

