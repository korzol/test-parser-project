This is a test app which main goal is to show OOP-fu and coding styles. It takes the following page https://www.jobs.bg/front_job_search.php?keyword=php&location_sid=1 parse it and output with power of print_r the following arrays:
    
    1. Job array - array of Job objects, which contain jobs details found on the page:
        a. job relative URL (job/123)
        b. job title
        c. date posted
        d. company relative URL (company/123)

    2. Company array - array of Company objects, which contain company details found on the page:
        a. company relative URL (company/123)
        b. company name
        c. full company logo url