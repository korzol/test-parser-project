<?php
declare(strict_types=1);

namespace VladApps\Jobsbg\Site;

final class CompanyCollection
{
    /**
     * @var array
     */
    private $collection = [];

    public function __construct(?array $companies = [])
    {
        foreach ($companies as $company)
        {
            $this->add($company);
        }
    }

    /**
     * Add instance of VladApps\Jobsbg\Site\Company into collection (array)
     * @param Company $company Instance of VladApps\Jobsbg\Site\Company
     */
    public function add(Company $company): void
    {
        $this->collection[$company->getCompanyUrl()] = $company;
    }

    /**
     * Remove defined Company instance from collection (array)
     * @param  Company $company Instance of VladApps\Jobsbg\Site\Company
     */
    public function remove(Company $company): void
    {
        unset($this->collection[$company->getCompanyUrl()]);
    }

    /**
     * Get an instance of VladApps\Jobsbg\Site\Company
     * @param  string $companyUrl Is a url like company/12345
     * @return VladApps\Jobsbg\Site\Company Instance of VladApps\Jobsbg\Site\Company
     */
    public function get(string $companyUrl): Company
    {
        return $this->collection[$companyUrl];
    }

    /**
     * Get Company collection (array)
     * @return array Company collection (array)
     */
    public function getAll(): array
    {
        return $this->collection;
    }
}