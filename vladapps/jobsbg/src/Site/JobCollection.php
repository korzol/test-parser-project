<?php
declare(strict_types=1);

namespace VladApps\Jobsbg\Site;

final class JobCollection
{
    /**
     * @var array
     */
    private $collection = [];

    public function __construct(?array $jobs = [])
    {
        foreach ($jobs as $job)
        {
            $this->add($job);
        }
    }

    /**
     * Add instance of VladApps\Jobsbg\Site\Job into collection (array)
     * @param Job $job Instance of VladApps\Jobsbg\Site\Job
     */
    public function add(Job $job): void
    {
        $this->collection[$job->getJobUrl()] = $job;
    }

    /**
     * Remove defined Job instance from collection (array)
     * @param  Job    $job Instance of VladApps\Jobsbg\Site\Job
     */
    public function remove(Job $job): void
    {
        unset($this->collection[$job->getJobUrl()]);
    }

    /**
     * Get an instance of VladApps\Jobsbg\Site\Job
     * @param  string $jobUrl Is a url like job/12345
     * @return VladApps\Jobsbg\Site\Job  Instance of VladApps\Jobsbg\Site\Job
     */
    public function get(string $jobUrl): Job
    {
        return $this->collection[$jobUrl];
    }

    /**
     * Get job collection (array)
     * @return array Job collection (array)
     */
    public function getAll(): array
    {
        return $this->collection;
    }
}