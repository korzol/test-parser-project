<?php

declare(strict_types=1);

namespace VladApps\Jobsbg\Site;

use DateTime;

final class Job
{
    /**
     * Like job/12345
     * @var string
     */
    private $jobUrl;

    /**
     * Job title
     * @var string
     */
    private $jobTitle;

    /**
     * Job title 2nd line - Месторабота София; Постоянна работа; Пълно работно време; Заплата от 4000 до 7500 BGN (Нето)
     * @var string
     */
    private $jobTitle2ndLine;

    /**
     * Job description. Only Available on job details page
     * @var string
     */
    private $jobBody;

    /**
     * Job viewed times. Only Available on job details page
     * @var string
     */
    private $jobViewed;

    /**
     * Job posted at - instance of php's DateTime
     * @var DateTime
     */
    private $jobPostedAt;

    /**
     * Job reference ID. Only Available on job details page
     * @var string
     */
    private $jobRefId;

    /**
     * URL like company/12345
     * @var string
     */
    private $companyId;

    /**
     * Static method. Initialize and return instance of self
     * @param  string      $jobUrl          [description]
     * @param  string      $jobTitle        [description]
     * @param  DateTime    $jobPostedAt     [description]
     * @param  string      $companyId       [description]
     * @param  string|null $jobTitle2ndLine [description]
     * @param  string|null $jobBody         [description]
     * @param  string|null $jobViewed       [description]
     * @param  string|null $jobRefId        [description]
     * @return VladApps\Jobsbg\Site\Job
     */
    public static function create(
        string      $jobUrl,
        string      $jobTitle,
        DateTime    $jobPostedAt,
        string      $companyId,
        ?string     $jobTitle2ndLine = null,
        ?string     $jobBody         = null,
        ?string     $jobViewed       = null,
        ?string     $jobRefId        = null
    ): self {
        return new self (
            $jobUrl,
            $jobTitle,
            $jobPostedAt,
            $companyId,
            $jobTitle2ndLine,
            $jobBody,
            $jobViewed,
            $jobRefId,
        );
    }

    /**
     * It is a getter for jobUrl
     * @return string
     */
    public function getjobUrl(): string
    {
        return $this->jobUrl;
    }

    /**
     * It is a getter for jobTitle
     * @return string
     */
    public function getjobTitle (): string
    {
        return $this->jobTitle;
    }

    /**
     * It is a getter for jobTitle2ndLine
     * @return string
     */
    public function getJobTitle2ndLine(): string
    {
        return $this->jobTitle2ndLine;
    }

    /**
     * It is a getter for jobPostedAt
     * @return string
     */
    public function getJobPostedAt()
    {
        return $this->jobPostedAt;
    }

    /**
     * It is a getter for companyId
     * @return string
     */
    public function getCompanyId(): string
    {
        return $this->companyId;
    }

    /**
     * It is a getter for jobBody
     * @return string
     */
    public function getJobBody()
    {
        return $this->jobBody;
    }

    /**
     * It is a getter for jobViewed
     * @return int
     */
    public function getJobViewed(): int
    {
        return $this->jobViewed;
    }

    /**
     * It is a getter for jobRefId
     * @return string
     */
    public function getJobRefId(): string
    {
        return $this->jobRefId;
    }

    /**
     * It is a setter for jobBody. Need it because initially job object creates
     * on job jisting page. Later we may need to add additional info from job
     * details page
     * @return void
     */
    public function setjobBody(string $jobBody): void
    {
        $this->jobBody = $jobBody;
    }

    /**
     * It is a setter for jobViewed. Need it because initially job object creates
     * on job jisting page. Later we may need to add additional info from job
     * details page
     * @return void
     */
    public function setJobViewed (int $jobViewed): void
    {
        $this->jobViewed = $jobViewed;
    }

    /**
     * It is a setter for jobRefId. Need it because initially job object creates
     * on job jisting page. Later we may need to add additional info from job
     * details page
     * @return void
     */
    public function setjobRefId (string $jobRefId): void
    {
        $this->jobRefId = $getjobRefId;
    }

    public function __construct(
        string      $jobUrl,
        string      $jobTitle,
        DateTime    $jobPostedAt,
        string      $companyId,
        string      $jobTitle2ndLine = null,
        ?string     $jobBody         = null,
        ?string     $jobViewed       = null,
        ?string     $jobRefId        = null
    ) {
        $this->jobUrl            = $jobUrl;
        $this->jobTitle          = $jobTitle;
        $this->jobPostedAt       = $jobPostedAt;
        $this->companyId        = $companyId;
        $this->jobTitle2ndLine   = $jobTitle2ndLine;
        $this->jobBody           = $jobBody;
        $this->jobViewed         = $jobViewed;
        $this->jobRefId          = $jobRefId;
    }
}