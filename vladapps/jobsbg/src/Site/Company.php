<?php
declare(strict_types=1);

namespace VladApps\Jobsbg\Site;

final class Company
{
    /**
     * URL like company/12345
     * @var string
     */
    private $companyUrl;

    /**
     * Company name
     * @var string
     */
    private $companyName;

    /**
     * Full URL to image associated with company
     * @var string
     */
    private $companyImageUrl;

    /**
     * Static method. Initialize and return instance of self
     * @param  string $companyUrl
     * @param  string $companyName
     * @param  string $companyImageUrl
     * @return self                    VladApps\Jobsbg\Site\Company
     */
    public static function create(
        string $companyUrl,
        string $companyName,
        string $companyImageUrl
    ): self {
        return new self(
            $companyUrl,
            $companyName,
            $companyImageUrl
        );
    }

    /**
     * It is a getter for companyUrl
     * @return string
     */
    public function getCompanyUrl(): string
    {
        return $this->companyUrl;
    }

    /**
     * It is a getter for companyName
     * @return string
     */
    public function getCompanyName(): string
    {
        return $this->companyName;
    }

    /**
     * It is a getter for companyImageUrl
     * @return string
     */
    public function getCompanyImageUrl(): string
    {
        return $this->companyImageUrl;
    }

    public function __construct(
        string $companyUrl,
        string $companyName,
        string $companyImageUrl
    ) {
        $this->companyUrl       = $companyUrl;
        $this->companyName      = $companyName;
        $this->companyImageUrl  = $companyImageUrl;
    }

}