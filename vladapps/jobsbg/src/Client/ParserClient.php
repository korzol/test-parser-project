<?php

namespace VladApps\Jobsbg\Client;

use DiDom\Document;
use VladApps\Jobsbg\Client\Parser\LoginParser;
use VladApps\Jobsbg\Client\Parser\JobListingsParser;
use VladApps\Jobsbg\Site\{JobCollection, CompanyCollection};

final class ParserClient
{
    /**
     * @var VladApps\Jobsbg\Client\Parser\JobListingsParser
     */
    private $jobListingsParser;

    public function __construct($html)
    {
        $this->jobListingsParser = new JobListingsParser( new Document($html) );
    }

    /**
     *
     * @return array Array of hidden login form fields
     */
    public function hiddenLoginFields(): array
    {
        $loginParser = new LoginParser();
        return $loginParser->buildHiddenFieldsArray($this->document);
    }

    /**
     * Return VladApps\Jobsbg\Site\JobCollection
     * @return array job listing collection
     */
    public function jobListing(): JobCollection
    {
        return $this->jobListingsParser->getJobCollection();
    }

    /**
     * Return VladApps\Jobsbg\Site\CompanyCollection
     * @return array job listing collection
     */
    public function companyListing(): CompanyCollection
    {
        return $this->jobListingsParser->getCompanyCollection();
    }
}