<?php
declare(strict_types=1);

namespace VladApps\Jobsbg\Client\Parser;

use DiDom\Document;

final class LoginParser
{
    /**
     * Parse provided html, find out hidden form fields and return array of them
     * @param  Document $document Instance of DiDom\Document
     * @return array Array of hidden login form fields
     */
    public function buildHiddenFieldsArray(Document $document): array
    {
        try {
            $fields = $document->find('input[type=hidden]');

            $i = 0;
            foreach($fields as $item) {
                // echo $item->value, "\n";
                $hiddenFields[$item->name] = $item->value;
                $i++;
            }

            return $hiddenFields;
        } catch(Exception $e) {
            throw new Exception("No hidden fields found. Make sure this is correct login form", 1);
        }
    }
}