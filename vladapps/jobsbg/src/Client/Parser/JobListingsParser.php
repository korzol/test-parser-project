<?php

namespace VladApps\Jobsbg\Client\Parser;

use DiDom\Document;
use VladApps\Jobsbg\Site\Job;
use VladApps\Jobsbg\Site\JobCollection;
use VladApps\Jobsbg\Site\Company;
use VladApps\Jobsbg\Site\CompanyCollection;
use DateTime;
use Exception;

final class JobListingsParser
{
    /**
     * $jobCollection Instance of JobCollection
     * @var JobCollection
     */
    private $jobCollection;

    /**
     * $companyCollection is an instance of CompanyCollection
     * @var CompanyCollection
     */
    private $companyCollection;

    public function __construct(Document $document)
    {
        $this->buildJobListings($document);
    }

    /**
     * Parse provided $document for job and company details and fill out $jobCollection and $companyCollection
     * @param  Document $document is instance of DiDom\Document
     * @return void
     */
    private function buildJobListings(Document $document): void
    {
        try {
            if ( $document->has('table[style="width:740px;"]') )
            {
                $fields = $document->find('table[style="width:740px;"]')[0]->find('tr');
                foreach ($fields as $item)
                {
                    if ( $item->child(1) && $item->child(1)->has('a.joblink') )
                    {
                        $selector = $item->child(3)->find('a')[0];

                        $jobUrl             = $item->child(1)->find('a.joblink')[0]->attr('href');
                        $jobTitle           = $item->child(1)->find('a.joblink')[0]->text();
                        $jobPostedAt        = $item->child(1)->find('span.explainGray')[0]->text();
                        $companyId          = $selector->attr('href');
                        $companyName        = $item->find('a.company_link')[0]->text();
                        $companyImageUrl    = $item->find('img')[0]->attr('src');

                        $companyUrlSplitted = explode('/', $companyId);
                        $companyUrl         = $companyUrlSplitted[3]."/".$companyUrlSplitted[4];
                        $companyID          = $companyUrlSplitted[4];

                        /*echo "
                                Link: {$jobUrl} \n
                                Title: {$jobTitle} \n
                                adPostedAt: {$jobPostedAt} \n
                                companyId : {$companyId} \n
                                companyUrl : {$companyUrl} \n
                                companyName : {$companyName} \n
                                companyImageUrl : {$companyImageUrl} \n
                            ";*/

                        $jobs[] = Job::create(
                            $jobUrl,
                            $jobTitle,
                            ( $jobPostedAt != 'днес' ? new DateTime($jobPostedAt) : new DateTime() ),
                            $companyUrl
                        );

                        $company[] = Company::create(
                            $companyUrl,
                            $companyName,
                            $companyImageUrl
                        );

                    }
                }

                $this->jobCollection        = new JobCollection($jobs);
                $this->companyCollection    = new CompanyCollection($company);
            }
            else
            {
                throw new Exception("Looks like provided \$document is not for this parser", 1);

            }
        } catch(Exception $e){
            die($e->getMessage().PHP_EOL);
        }
    }

    /**
     * Contains job details found on job listing page
     * @return JobCollection Instance of VladApps\Jobsbg\site\JobCollection
     */
    public function getJobCollection(): JobCollection
    {
        return $this->jobCollection;
    }

    /**
     * Contains company details found on job listing page
     * @return CompanyCollection Instance of VladApps\Jobsbg\Site\CompanyCollection
     */
    public function getCompanyCollection(): CompanyCollection
    {
        return $this->companyCollection;
    }
}