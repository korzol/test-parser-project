<?php
declare(strict_types=1);

namespace VladApps\Jobsbg;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;

use VladApps\Jobsbg\Client\ParserClient;

use GuzzleHttp\Psr7;
// use GuzzleHttp\Exception\ClientException;

//use VladApps\Jobsbg\Exception\UnprocessableEntityException;

class GuzzleJobsbgClient implements JobsbgClientInterface
{
    /**
     * @var ClientInterface|Client
     */
    private $guzzleClient;

    /**
     * @var string
     */
    private $userEmail;

    /**
     * @var string
     */
    private $userPassword;

    /**
     * @var string
     */
    protected $user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36 OPR/66.0.3515.27';


    public function __construct(ClientInterface $guzzleClient, string $userEmail, string $userPassword, $user_agent=null)
    {
        $this->guzzleClient     = $guzzleClient;
        $this->userEmail        = $userEmail;
        $this->userPassword     = $userPassword;
        ! is_null($user_agent)  ? $this->user_agent = $user_agent : false;
    }

    public function userLogin(): ResponseInterface
    {
        $url = self::SITE_BASE_URL . self::SITE_AUTHORIZE_USER_URL;

        $html = (string)$this->get($url)->getBody();

        $parseClient = new ParserClient($html);
        $fieldsArray = $parseClient->hiddenLoginFields();

        $loginData = [
            'username' => $this->userEmail,
            'pass' => $this->userPassword,
        ];

        $formData['form_params'] = array_merge($fieldsArray, $loginData);

        return $this->post($url, $formData);
    }

    public function jobsListing(): ParserClient
    {
        $url = self::SITE_BASE_URL . self::SITE_JOB_LISTING_URL;

        $html = (string)$this->get($url)->getBody();

        return new ParserClient($html);
    }

    public function get(string $url, ?array $parameters = null): ResponseInterface
    {
        $headers = ['headers' => ['USER-AGENT' => $this->user_agent]];
        if ( ! is_null($parameters) )
        {
            $options = array_merge($headers, $parameters);
        }
        else
        {
            $options = $headers;
        }

        try {
            return $this->guzzleClient->get($url, $options);
        } catch(ClientException $e) {
            echo Psr7\str($e->getRequest());
            // echo Psr7\str($e->getResponse());
            echo "Something went wrong while requesting this URL \n";
            exit();

        }
    }

    public function post(string $url, array $data): ResponseInterface
    {
        $headers = ['headers' => ['USER-AGENT' => $this->user_agent]];
        $options = array_merge($headers, $data);

        try {
            return $this->guzzleClient->post($url, $options);
        } catch(ClientException $exception) {
            echo Psr7\str($e->getRequest());
            // echo Psr7\str($e->getResponse());
            echo "Something went wrong while POSTing to this URL \n";
        }

    }
}