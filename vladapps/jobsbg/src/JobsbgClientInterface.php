<?php

namespace VladApps\Jobsbg;

use Psr\Http\Message\ResponseInterface;
use VladApps\Jobsbg\Client\ParserClient;

interface JobsbgClientInterface
{
    const SITE_BASE_URL             = 'https://www.jobs.bg';

    const SITE_AUTHORIZE_USER_URL   = '/login.php';

    const SITE_JOB_LISTING_URL      = '/front_job_search.php?keyword=php&location_sid=1';

    public function userLogin(): ResponseInterface;
    public function jobsListing(): ParserClient;
}